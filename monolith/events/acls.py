from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_photo(city, state):
    # Set the headers for the Pexels 
    headers = {"Authorization": PEXELS_API_KEY}
    
    # Prepare the query parameters for the photo search.
    photo_query = {'query': f'{city}, {state}', 'per_page': 1}
    
    # Define the URL for the Pexels API.
    url = "https://api.pexels.com/v1/search"

    # Send a GET request to the Pexels API with the specified query and headers.
    response = requests.get(url, photo_query, headers=headers)
    
    # Parse the JSON response from the API.
    content = response.json()
    
    # Extract the URL of the first photo in the response.
    picture_url = content["photos"][0]["src"]["original"]
    
    # Return the URL of the photo.
    return picture_url

# This function fetches weather information for a city and state using the OpenWeatherMap API.
def get_weather(city, state):
    # Set the headers for the OpenWeatherMap API request with an authorization token.
    # headers = {"Authorization": OPEN_WEATHER_API_KEY}
    
    # Prepare the query parameters to get location data based on city and state.
    loc_params = {'q': f'{city}, {state}, US', "limit": 1, "appid": OPEN_WEATHER_API_KEY}
    
    # Define the URL for the OpenWeatherMap location API.
    loc_url = 'http://api.openweathermap.org/geo/1.0/direct'
    
    # Send a GET request to the location API with the specified query and headers.
    loc_response = requests.get(loc_url, params=loc_params)
    
    # Parse the JSON response from the location API.
    loc_content = loc_response.json()
    
    # E latitude and longitude coordinates from the location data.
    coordinates = {
        "lat": loc_content[0]["lat"],
        "lon": loc_content[0]["lon"]
    }

    #  query parameters for fetching weather data using coordinates.
    weather_params = {
        "lat": coordinates["lat"],
        "lon": coordinates["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    
    # Define the URL for the OpenWeatherMap weather API.
    weather_url = 'https://api.openweathermap.org/data/2.5/weather'
    
    # Send a GET request to the weather API with the specified query and headers.
    response = requests.get(weather_url, params=weather_params)
    
    # Parse the JSON response from the weather API.
    content = response.json()
    
    # Extract and format weather information such as description and temperature.
    weather = {
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"]
    } 
    
    # Return the weather information.
    return weather